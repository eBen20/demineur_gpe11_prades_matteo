import org.junit.Assert;

public class TestCase {

	@Test
	public void unTest() {
		Case case4 = new Case();
		Case case1 = new Case();
		Case case2 = new Case();
		Case case3 = new Case();
        case4.ajouteCaseVoisine(case1);
        case4.ajouteCaseVoisine(case2);
        case4.ajouteCaseVoisine(case3);
        
        assertTrue(! case1.estBombe());
        assertTrue(! case1.estMarquee());
        assertTrue(! case3.estRevelee());
        case1.ajouteBombe(); case1.reveler(); case3.marquer();
        assertTrue(case1.estBombe());
        assertTrue(case1.estMarquee());
        assertTrue(case3.estRevelee());

        assertEquals(getNbBombesVoisines(), 1);
    }
}