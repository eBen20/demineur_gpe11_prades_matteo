import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Tests {
    private Grille g1;
    private Grille g2;
    private Grille g3;
    private Case case1; 
    private Case case2; 
    private Case case3; 
    private Case case4; 
    @Before
    public void init() {
        this.g1 = new Grille(0, 0);
        this.g2 = new Grille(3, 2);
        this.g3 = new Grille(5, 5);
        this.case4 = new Case();
		this.case1 = new Case();
		this.case2 = new Case();
		this.case3 = new Case();
        case4.ajouteCaseVoisine(case1);
        case4.ajouteCaseVoisine(case2);
        case4.ajouteCaseVoisine(case3);
    }

	@Test
	public void test() {
	
        
        assertTrue(! case1.estBombe());
        assertTrue(! case1.estMarquee());
        assertTrue(! case3.estRevelee());
        case1.ajouteBombe(); case1.reveler(); case3.marquer();
        assertTrue(case1.estBombe());
        assertTrue(case1.estMarquee());
        assertTrue(case3.estRevelee());

    }

    @Test
    public void test_getNbLignes(){

        assertEquals(g1.getNbLignes(), 0);
        assertEquals(g2.getNbLignes(), 3);
        assertEquals(g3.getNbLignes(), 5);
       
    }

    @Test
    public void test_getNbColonnes(){

        assertEquals(g1.getNbColonnes(), 0);
        assertEquals(g2.getNbColonnes(), 2);
        assertEquals(g3.getNbColonnes(), 5);
       
    }    
    
    @Test
    public void test_getNbBombes(){

        assertEquals(g1.getNbBombes(), 0);
        assertEquals(g2.getNbBombes(), 2);
        assertEquals(g3.getNbBombes(), 4);
       
    }

    @Test
    public void test_getCase(){

        assertEquals(g1.getCase(0, 0), null);
        assertEquals(g2.getCase(1, 1), new Case());
        assertEquals(g3.getCase(2, 3), new Case());
       
    }
    
}