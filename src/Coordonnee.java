public class Coordonnee {
    private int x;
    private int y;

    public Coordonnee(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if( obj == null){ return false;}
        if( obj == this){ return true;}
        if( !(obj instanceof Coordonnee)){ return false;}
        Coordonnee co = (Coordonnee) obj;
        if( this.x == co.x && this.y == co.y){return true;}
        else{ return false;}
    }

    @Override
    public int hashCode() {
        return this.x * 31 + this.y * 3;
    }
}
